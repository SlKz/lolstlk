import express = require('express');
import { Map } from 'immutable';
const Discord = require('discord.io');
const app: express.Application = express();

import { getSummonerByName } from "./services/riot-api/server-riot-service";
const auth = require('./auth.json');

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, async () => {
  console.log('Example app listening on port 3000!');
  await bot_work();
});

const getCmdRoutes: Map<string, any> = Map<string, any>()
  .set('summoner', (summonerName: string) => getSummonerByName(summonerName));

const bot_work = async () => {
  const bot = new Discord.Client({
    token: auth.token,
    autorun: true
  });

  bot.on('ready', () => {
    console.log(`Connected as ${bot.username}`);
  });

  await bot.on('message', (user: any, userId: any, channelId: string, message: string, evt: any) => {
    if (userId !== bot.id
      && channelId === '553343845849366547'
      && message.slice(0, 2) === 'ls') {
      const msgSplit: Array<string> = message.split(' ');
      bot.simulateTyping(
        channelId,
        async () => {
          bot.sendMessage({
            to: channelId,
            message:
              getCmdRoutes.has(msgSplit[1])
                ? await getCmdRoutes.get(msgSplit[1])(msgSplit[2])
                : `no commands found`
          });
        });
    } else {
      console.log(userId, 'send:', message);
    }
  });
};
